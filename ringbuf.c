/*
  cpumond - yet another CPU monitor daemon
  Copyright (C) 2017 Jaromir Capik <jaromir.capik@email.cz>

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/


#include <stdlib.h>
#include <syslog.h>
#include <pthread.h>
#include "ringbuf.h"
#include "common.h"


void ring_buffer_destroy(ring_buffer_t *ring_buffer)
{
  safe_free(ring_buffer->toplist_args);
  safe_free(ring_buffer->toplist);
  safe_free(ring_buffer->ts);

  if (ring_buffer->cpu_sample) {
    safe_free(ring_buffer->cpu_sample);
    pthread_mutex_destroy(&ring_buffer->mutex);
  }
}


int ring_buffer_init(ring_buffer_t *ring_buffer, config_t *config)
{
  if (pthread_mutex_init(&ring_buffer->mutex, NULL)) {
    syslog(LOG_ERR, "%s: pthread_mutex_init failed!", __func__);
    return RET_FAIL;
  }

  ring_buffer->reader_index = 0;
  ring_buffer->writer_index = 0;
  ring_buffer->config = config;

  ring_buffer->cpu_sample = (cpu_sample_t *) malloc(config->core_count * config->sample_count * sizeof(cpu_sample_t));
  if (ring_buffer->cpu_sample) {

    ring_buffer->ts = (time_t *) malloc(config->sample_count * sizeof(time_t));
    if (ring_buffer->ts) {

      ring_buffer->toplist = (toplist_t *) malloc(config->toplist_length * config->sample_count * sizeof(toplist_t));
      if (ring_buffer->toplist) {

        ring_buffer->toplist_args = (char *) malloc(config->toplist_length * config->arg_size * config->sample_count * sizeof(char));
        if (ring_buffer->toplist_args)
          return RET_OK;

      }
    }
  }

  syslog(LOG_ERR, "%s: malloc failed!", __func__);
  ring_buffer_destroy(ring_buffer);

  return RET_FAIL;
}
