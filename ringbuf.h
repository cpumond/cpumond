/*
  cpumond - yet another CPU monitor daemon
  Copyright (C) 2017 Jaromir Capik <jaromir.capik@email.cz>

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef __RINGBUF_H__
#define __RINGBUF_H__


#include <pthread.h>
#include "config.h"
#include "proc.h"


#define FLAG_PROC_ARRAY_FULL	(1UL << 0)
#define FLAG_PROC_READ_ERROR	(1UL << 1)


typedef struct {
  unsigned int reader_index;
  unsigned int writer_index;
  config_t *config;
  time_t *ts;
  cpu_sample_t *cpu_sample;
  toplist_t *toplist;
  char *toplist_args;
  unsigned long flags;
  pthread_mutex_t mutex;
} ring_buffer_t;


int ring_buffer_init(ring_buffer_t *ring_buffer, config_t *config);
void ring_buffer_destroy(ring_buffer_t *ring_buffer);


#endif
