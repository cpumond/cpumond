/*
  cpumond - yet another CPU monitor daemon
  Copyright (C) 2017 Jaromir Capik <jaromir.capik@email.cz>

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#define _GNU_SOURCE

#include <time.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <linux/limits.h>
#include "daemonize.h"
#include "ringbuf.h"
#include "config.h"
#include "logger.h"
#include "common.h"
#include "proc.h"


static cpu_sample_t *cpu_reader_buffer = NULL;
static toplist_t *toplist_reader_buffer = NULL;
static char *toplist_arg_reader_buffer = NULL;
static FILE *f_samples = NULL;
static FILE *f_log = NULL;
static char path[PATH_MAX];


static int cpu_sample_write(cpu_sample_t *cpu_sample, time_t *ts, unsigned int core_count)
{
  fwrite(ts, sizeof(time_t), 1, f_samples); // FIXME check
  fwrite(cpu_sample, sizeof(cpu_sample_t), core_count, f_samples); // FIXME check

  return RET_OK;
}


// open a new blob for writing and write the header
static int cpu_blob_init(config_t *config)
{
  time_t ts;
  struct tm *lt;

  time(&ts);
  lt = localtime(&ts);

  snprintf(path, sizeof(path), "%s_%04u%02u%02u_%02u%02u%02u", config->blob_filename,
	   lt->tm_year+1900, lt->tm_mon+1, lt->tm_mday, lt->tm_hour, lt->tm_min, lt->tm_sec);

  f_samples = fopen(path, "wb");

  if (!f_samples) {
    syslog(LOG_ERR, "%s: fopen failed!", __func__);
    return RET_FAIL;
  }

  fwrite(&config->core_count, sizeof(config->core_count), 1, f_samples); // FIXME check

  return RET_OK;
}


static int log_current_timestamp()
{
  struct tm *lt;
  time_t cur_ts;

  time(&cur_ts);
  lt = localtime(&cur_ts);
  fprintf(f_log, "lt %04u-%02u-%02u %02u:%02u:%02u\n",
	  lt->tm_year+1900, lt->tm_mon+1, lt->tm_mday, lt->tm_hour, lt->tm_min, lt->tm_sec);
  return RET_OK;
}


static int toplist_sample_write(toplist_t *toplist, char *toplist_arg, cpu_sample_t *cpu_sample, time_t *ts,
				unsigned int toplist_length, unsigned int arg_size, unsigned int core_count)
{
  unsigned int tl_index, core;
  struct tm *lt;

  // timestamps
  lt = localtime(ts);
  fprintf(f_log, "st %04u-%02u-%02u %02u:%02u:%02u\n", 
	  lt->tm_year+1900, lt->tm_mon+1, lt->tm_mday, lt->tm_hour, lt->tm_min, lt->tm_sec);
  (void)log_current_timestamp();

  // core loads
  fprintf(f_log, "cl");
  for (core = 0; core < core_count; core++)
    fprintf(f_log, " %d", 100 - cpu_sample[core].idle);
  fprintf(f_log, "\n");

  // top processes
  for (tl_index = 0; tl_index < toplist_length; tl_index++) {
    if (!toplist[tl_index].pid) break;
    fprintf(f_log, "#%d %d %d %d %d %d %d %d %d %d %s\n", tl_index+1, toplist[tl_index].pid, toplist[tl_index].ppid,
	    toplist[tl_index].time, toplist[tl_index].utime, toplist[tl_index].stime, toplist[tl_index].cutime,
	    toplist[tl_index].cstime, toplist[tl_index].guest_time, toplist[tl_index].cguest_time, toplist_arg + tl_index * arg_size);
  }

  // separator
  fprintf(f_log, "---\n");

  return RET_OK;
}


// open a new log for writing
static int log_init(config_t *config)
{
  time_t ts;
  struct tm *lt;

  time(&ts);
  lt = localtime(&ts);

  snprintf(path, sizeof(path), "%s_%04u%02u%02u_%02u%02u%02u", config->log_filename,
	   lt->tm_year+1900, lt->tm_mon+1, lt->tm_mday, lt->tm_hour, lt->tm_min, lt->tm_sec);

  f_log = fopen(path, "w");

  if (!f_log) {
    syslog(LOG_ERR, "%s: fopen failed!", __func__);
    return RET_FAIL;
  }

  return RET_OK;
}


void logger_destroy()
{
  safe_fclose(f_log);
  safe_fclose(f_samples);

  safe_free(toplist_arg_reader_buffer);
  safe_free(toplist_reader_buffer);
  safe_free(cpu_reader_buffer);
}


int logger_init(config_t *config)
{
  cpu_reader_buffer = (cpu_sample_t *) malloc(config->core_count * sizeof(cpu_sample_t));
  if (!cpu_reader_buffer) {
    syslog(LOG_ERR, "%s: malloc failed!", __func__);
  } else {

    toplist_reader_buffer = (toplist_t *) malloc(config->toplist_length * sizeof(toplist_t));
    if (!toplist_reader_buffer) {
      syslog(LOG_ERR, "%s: malloc failed!", __func__);
    } else {

      toplist_arg_reader_buffer = (char *) malloc(config->toplist_length * config->arg_size * sizeof(char));
      if (!toplist_arg_reader_buffer) {
	syslog(LOG_ERR, "%s: malloc failed!", __func__);
      } else {

	if (cpu_blob_init(config))
	  if (log_init(config))
	    return RET_OK;

      }
    }
  }

  logger_destroy();

  return RET_FAIL;
}


int finalize(time_t *ts, config_t *config)
{
  static int mday = -1;
  struct tm *lt;
  int ret = RET_OK;

  lt = localtime(ts);

  if (unlikely(unlikely(lt->tm_mday != mday) && likely(mday != -1))) {  // new day -> new files

    ret = RET_FAIL;

    safe_fclose(f_log);
    safe_fclose(f_samples);

    if (cpu_blob_init(config))
      if (log_init(config))
        ret = RET_OK;

  }

  mday = lt->tm_mday;

  return ret;
}


int logger(ring_buffer_t *ring_buffer)
{
  unsigned int core;
  unsigned int loaded_cores;
  unsigned int tl_index;
  unsigned long flags;
  time_t ts;

  // log till we receive a SIGTERM or SIGINT
  while (likely(!daemon_terminated())) {

    sleep(ring_buffer->config->logging_interval);

    pthread_mutex_lock(&ring_buffer->mutex);

    // copy all flags & clean flags targetted to logger
    flags = ring_buffer->flags;
    ring_buffer->flags &= ~(FLAG_PROC_ARRAY_FULL | FLAG_PROC_READ_ERROR);

    pthread_mutex_unlock(&ring_buffer->mutex);

    if (flags & (FLAG_PROC_ARRAY_FULL | FLAG_PROC_READ_ERROR)) { // any flag targetted to logger?
      (void)log_current_timestamp();
      if (flags & FLAG_PROC_ARRAY_FULL)
	fprintf(f_log, "WARN: Too many running processes!\n");
      if (flags & FLAG_PROC_READ_ERROR)
	fprintf(f_log, "ERROR: Can't read proc entries!\n");
      // separator
      fprintf(f_log, "---\n");
    }

    pthread_mutex_lock(&ring_buffer->mutex);

    // process new entries in the ring buffer (write them and do heuristics & stats)
    while ((ring_buffer->reader_index != ring_buffer->writer_index) && likely(!daemon_terminated())) {

      // copy one cpu sample from the ring buffer to the cpu reader buffer
      for (core = 0; core < ring_buffer->config->core_count ; core++)
        cpu_reader_buffer[core] = ring_buffer->cpu_sample[ring_buffer->reader_index * ring_buffer->config->core_count + core];

      // copy one toplist sample from the ring buffer to the toplist reader buffer
      for (tl_index = 0; tl_index < ring_buffer->config->toplist_length ; tl_index++) {
        toplist_reader_buffer[tl_index] = ring_buffer->toplist[ring_buffer->reader_index * ring_buffer->config->toplist_length + tl_index];
        strcpy(toplist_arg_reader_buffer + tl_index * ring_buffer->config->arg_size,
               ring_buffer->toplist_args + (ring_buffer->reader_index * ring_buffer->config->toplist_length + tl_index) * ring_buffer->config->arg_size);
      }

      // get a copy of the sample timestamp
      ts = ring_buffer->ts[ring_buffer->reader_index];

      ring_buffer->reader_index++; ring_buffer->reader_index %= ring_buffer->config->sample_count;

      pthread_mutex_unlock(&ring_buffer->mutex);

      // process the copied data
      (void)cpu_sample_write(cpu_reader_buffer, &ts, ring_buffer->config->core_count);

      loaded_cores = 0;
      for (core = 0; core < ring_buffer->config->core_count ; core++)
        if (cpu_reader_buffer[core].idle <= 100 - ring_buffer->config->load_threshold) loaded_cores++;

      if ((100 * loaded_cores / ring_buffer->config->core_count) >= ring_buffer->config->core_threshold)  // the system is loaded -> write the process log
        (void)toplist_sample_write(toplist_reader_buffer, toplist_arg_reader_buffer, cpu_reader_buffer,
                                   &ts, ring_buffer->config->toplist_length, ring_buffer->config->arg_size,
                                   ring_buffer->config->core_count);


      if (!finalize(&ts, ring_buffer->config)) {
	// we failed to create output files -> try syslog & terminate
	syslog(LOG_CRIT, "%s: Unable to create output file(s)! Terminating!", __func__);
	return RET_FAIL;
      }

      pthread_mutex_lock(&ring_buffer->mutex);
    }

    pthread_mutex_unlock(&ring_buffer->mutex);

    fflush(f_samples);
    fflush(f_log);
  }

  return RET_OK;
}
