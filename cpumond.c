/*
  cpumond - yet another CPU monitor daemon
  Copyright (C) 2017 Jaromir Capik <jaromir.capik@email.cz>

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#define _GNU_SOURCE

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/mman.h>
#include <sys/types.h>
#include "daemonize.h"
#include "ringbuf.h"
#include "sampler.h"
#include "logger.h"
#include "config.h"
#include "proc.h"


int main(int argc, char *argv[])
{
  ring_buffer_t ring_buffer;
  config_t config;
  int ret = EXIT_FAILURE;

  // get hw & sw config
  config_get(&config, argc, argv);

  // become a daemon
  daemonize();

  // init the required structures
  if (ring_buffer_init(&ring_buffer, &config)) {

    // ...
    if (proc_init(&config)) {

      // ...
      if (logger_init(&config)) {

	// prevent the currently allocated memory
	// from being swapped (root permissions needed)
	if (!mlockall(MCL_CURRENT) || MLOCK_NOT_REQUIRED) {

	  // start the sampler thread
	  if (sampler_thread_start(&ring_buffer)) {

	    // enter the main logging loop
	    if (logger(&ring_buffer))
	      ret = EXIT_SUCCESS;

	  }

	}

	// do some cleaning
	logger_destroy();
      }

      // ...
      proc_destroy();
    }

    // ...
    ring_buffer_destroy(&ring_buffer);
  }

  return ret;
}
