/*
  cpumond - yet another CPU monitor daemon
  Copyright (C) 2017 Jaromir Capik <jaromir.capik@email.cz>

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#define _GNU_SOURCE

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include "config.h"


static void usage(int opt)
{
  int err = (opt == '?') || (opt == ':');
  FILE *fp = err ? stderr : stdout;

  fputs("\nUsage:\n", fp);
  fprintf(fp, " %s [options]\n", program_invocation_short_name);
  fputs("\nOptions:\n", fp);
  fputs(" -a, --arg-size=N              limit length of the argument vector to N\n", fp);
  fputs(" -b, --blob-filename=F         set F as the output filename for cpu samples\n", fp);
  fputs(" -c, --core-threshold=N        set the max. number of loaded cores to N %\n", fp);
  fputs(" -i, --logging-interval=N      set the interval of log writing to N seconds\n", fp);
  fputs(" -I, --sampling-interval=N     set the interval of sampling to N seconds\n", fp);
  fputs(" -l, --log-filename=F          set F as the output filename for logs\n", fp);
  fputs(" -m, --load-threshold=N        set the max. load per core to N %\n", fp);
  fputs(" -p, --proc-count=N            set the max. number of expected processes to N\n", fp);
  fputs(" -s, --sample-count=N          set the cpu ring buffer size to N samples\n", fp);
  fputs(" -t, --toplist-length=N        set the maximum toplist length to N processes\n", fp);
  fputs(" -h, --help                    show this help\n", fp);
  fputs("\n", fp);

  exit(err ? EXIT_FAILURE : EXIT_SUCCESS);
}


void config_get(config_t *config, int argc, char *argv[])
{
  const char *opts = "a:b:c:i:I:l:m:p:s:t:h";
  const struct option longopts[] = {
    {"arg-size", required_argument, NULL, 'a'},
    {"blob-filename", required_argument, NULL, 'b'},
    {"core-threshold", required_argument, NULL, 'c'},
    {"logging-interval", required_argument, NULL, 'i'},
    {"sampling-interval", required_argument, NULL, 'I'},
    {"log-filename", required_argument, NULL, 'l'},
    {"load-threshold", required_argument, NULL, 'm'},
    {"proc-count", required_argument, NULL, 'p'},
    {"sample-count", required_argument, NULL, 's'},
    {"toplist-length", required_argument, NULL, 't'},
    {"help", no_argument, NULL, 'h'},
    {NULL, 0, NULL, 0}
  };
  int opt;
  int tmp;

  // include hardware config
  config->core_count = sysconf(_SC_NPROCESSORS_CONF);

  // set config defaults
  config->proc_count = DEFAULT_PROC_COUNT;
  config->sample_count = DEFAULT_SAMPLE_COUNT;
  config->blob_filename = DEFAULT_SAMPLE_BLOB_FILENAME;
  config->log_filename = DEFAULT_LOG_FILENAME;
  config->toplist_length = DEFAULT_TOPLIST_LENGTH;
  config->arg_size = DEFAULT_ARG_SIZE;
  config->core_threshold = DEFAULT_CORE_THRESHOLD;
  config->load_threshold = DEFAULT_LOAD_THRESHOLD;
  config->logging_interval = DEFAULT_LOGGING_INTERVAL;
  config->sampling_interval = DEFAULT_SAMPLING_INTERVAL;

  // process command line arguments to override config defaults
  while ((opt = getopt_long (argc, argv, opts, longopts, NULL)) != -1) {
    switch (opt) {
      case 'a':
	tmp = atoi(optarg);
	if (tmp < MIN_ARG_SIZE) {
	  fprintf(stderr, "%s: wrong argument size - need an integer higher than %d!\n",
		  program_invocation_name, MIN_ARG_SIZE - 1);
	  usage ('?');
	}
	config->arg_size = tmp;
	break;
      case 'b':
	config->blob_filename = optarg;
	break;
      case 'c':
	tmp = atoi(optarg);
	if ((tmp < 0) || (tmp > 100)) {
	  fprintf(stderr, "%s: wrong cpu threshold - need an integer between 0 and 100!\n",
		  program_invocation_name);
	  usage ('?');
	}
	config->core_threshold = tmp;
	break;
      case 'i':
	tmp = atoi(optarg);
	if (tmp < MIN_LOGGING_INTERVAL) {
	  fprintf(stderr, "%s: wrong logging interval - need an integer higher than %d!\n",
		  program_invocation_name, MIN_LOGGING_INTERVAL - 1);
	  usage ('?');
	}
	config->logging_interval = tmp;
	break;
      case 'I':
	tmp = atoi(optarg);
	if (tmp < MIN_SAMPLING_INTERVAL) {
	  fprintf(stderr, "%s: wrong sampling interval - need an integer higher than %d!\n",
		  program_invocation_name, MIN_SAMPLING_INTERVAL - 1);
	  usage ('?');
	}
	config->sampling_interval = tmp;
	break;
      case 'l':
	config->log_filename = optarg;
	break;
      case 'm':
	tmp = atoi(optarg);
	if ((tmp < 0) || (tmp > 100)) {
	  fprintf(stderr, "%s: wrong load threshold - need an integer between 0 and 100!\n",
		  program_invocation_name);
	  usage ('?');
	}
	config->load_threshold = tmp;
	break;
      case 'p':
	tmp = atoi(optarg);
	if (tmp < MIN_PROC_COUNT) {
	  fprintf(stderr, "%s: wrong process count - need an integer higher than %d!\n",
		  program_invocation_name, MIN_PROC_COUNT - 1);
	  usage ('?');
	}
	config->proc_count = tmp;
	break;
      case 's':
	tmp = atoi(optarg);
	if (tmp < MIN_SAMPLE_COUNT) {
	  fprintf(stderr, "%s: wrong sample count - need an integer higher than %d!\n",
		  program_invocation_name, MIN_SAMPLE_COUNT - 1);
	  usage ('?');
	}
	config->sample_count = tmp;
	break;
      case 't':
	tmp = atoi(optarg);
	if (tmp < MIN_TOPLIST_LENGTH) {
	  fprintf(stderr, "%s: wrong toplist length - need an integer higher than %d!\n",
		  program_invocation_name, MIN_TOPLIST_LENGTH - 1);
	  usage ('?');
	}
	config->toplist_length = tmp;
	break;
      case 'h':
      case '?':
      case ':':
	usage (opt);
    }
  }

  if (argc > optind) {
    fprintf(stderr, "%s: extra argument found -- '%s'\n",
	    program_invocation_name, argv[optind]);
    usage ('?');
  }
}
