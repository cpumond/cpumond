/*
  cpumond - yet another CPU monitor daemon
  Copyright (C) 2017 Jaromir Capik <jaromir.capik@email.cz>

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#define _GNU_SOURCE

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <syslog.h>
#include <string.h>
#include "daemonize.h"


static int terminate = 0;


static void sig_handle(int sig)
{
  switch(sig) {
    case SIGINT:
    case SIGTERM:
      terminate = 1;
      break;
    default:
      syslog(LOG_WARNING, "%s: %s caught", __func__, strsignal(sig));
      break;
  }
}


inline int daemon_terminated()
{
  return terminate;
}


void daemonize()
{
  pid_t pid;
  struct sigaction sigact;
  sigset_t sigblk;

  pid = fork();

  if (pid < 0) {
    perror("fork");
    exit(EXIT_FAILURE);
  } else if (pid > 0) {
    exit(EXIT_SUCCESS);
  }

  if (setsid() < 0) {
    perror("setsid");
    exit(EXIT_FAILURE);
  }

  // close standard descriptors
  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

  // block the following signals
  sigemptyset(&sigblk);
  sigaddset(&sigblk, SIGCHLD);
  sigaddset(&sigblk, SIGTSTP);
  sigaddset(&sigblk, SIGTTOU);
  sigaddset(&sigblk, SIGTTIN);
  sigprocmask(SIG_BLOCK, &sigblk, NULL);

  // catch the following signals
  sigact.sa_handler = sig_handle;
  sigemptyset(&sigact.sa_mask);
  sigact.sa_flags = 0;
  sigaction(SIGHUP, &sigact, NULL);
  sigaction(SIGTERM, &sigact, NULL);
  sigaction(SIGINT, &sigact, NULL);

  // init syslog
  setlogmask(LOG_UPTO(LOG_INFO));
  openlog(program_invocation_short_name, LOG_CONS, LOG_DAEMON);
}
