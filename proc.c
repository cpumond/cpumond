/*
  cpumond - yet another CPU monitor daemon
  Copyright (C) 2017 Jaromir Capik <jaromir.capik@email.cz>

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <dirent.h>
#include "ringbuf.h"
#include "config.h"
#include "proc.h"
#include "common.h"


static cpu_jiffies_t *jiffies_old;
static cpu_jiffies_t *jiffies_new;
static proc_entry_t *proc_entries[2]; // old + new (=2)
static proc_entry_t *proc_entries_diff;
static char *proc_args[2];
static char **proc_args_diff;
static int proc_entries_new_toggle;   // old/new entries switcher
static char path[PATH_MAX];


static int cpu_jiffies_get(cpu_jiffies_t *jiffies, unsigned int core_count)
{
  FILE *f;
  unsigned int core;
  int num;
  static char line[MAX_LINE_LEN];

  f = fopen("/proc/stat", "r");
  if (unlikely(!f)) {
    return RET_FAIL;
  }

  // drop the totals
  if (unlikely(!fgets(line, sizeof(line), f))) {
    fclose(f);
    return RET_FAIL;
  }

  for (core = 0; core < core_count; core++) {
    if (unlikely(!fgets(line, sizeof(line), f))) {
      fclose(f);
      return RET_FAIL;
    }
    #define ELEMENTS_TO_READ 10
    num = sscanf(line, "%*s %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu",
      &jiffies[core].user,
      &jiffies[core].nice,
      &jiffies[core].system,
      &jiffies[core].idle,
      &jiffies[core].iowait,
      &jiffies[core].irq,
      &jiffies[core].softirq,
      &jiffies[core].steal,
      &jiffies[core].guest,
      &jiffies[core].guest_nice
    );
    if (unlikely(num < ELEMENTS_TO_READ)) {
      fclose(f);
      return RET_FAIL;
    }
    #undef ELEMENTS_TO_READ
  }

  fclose(f);

  return RET_OK;
}


static int proc_entry_get(proc_entry_t *entry, char *arg, unsigned int arg_size, char *pid_str)
{
  FILE *f;
  int c;
  char *pos;
  int num;
  static char line[MAX_LINE_LEN];

  // read the process stat
  snprintf(path, sizeof(path), "/proc/%s/stat", pid_str);

  f = fopen(path, "r");
  if (unlikely(!f)) {
    return RET_FAIL;
  }

  if (unlikely(!fgets(line, sizeof(line), f))) {
    fclose(f);
    return RET_FAIL;
  }

  #define ELEMENTS_TO_READ 8
  num = sscanf(line, "%d %*s %*c %d %*s %*s %*s %*s %*s %*s %*s %*s %*s %lu %lu %ld %ld "
		     "%*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s "
		     "%*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %lu %ld",
		     &entry->pid,&entry->ppid,&entry->utime,&entry->stime,&entry->cutime,
		     &entry->cstime,&entry->guest_time,&entry->cguest_time);

  fclose(f);

  if (unlikely(num < ELEMENTS_TO_READ)) {
    return RET_FAIL;
  }
  #undef ELEMENTS_TO_READ

  // read the argument vector
  snprintf(path, sizeof(path), "/proc/%s/cmdline", pid_str);

  f = fopen(path, "r");
  if (!f) {
    return RET_FAIL;
  }

  pos = arg;
  while (((c = fgetc(f)) != EOF) && (pos < (arg + arg_size - 1))) {
    if (c == '\0') c = ' ';
    *pos = c;
    pos++;
  }
  *pos = '\0';

  fclose(f);

  if (!strlen(arg)) { // empty argument vector? -> fallback to comm

    snprintf(path, sizeof(path), "/proc/%s/comm", pid_str);

    f = fopen(path, "r");
    if (!f) {
      return RET_FAIL;
    }

    pos = arg; *pos = '['; pos++;
    while (((c = fgetc(f)) != EOF) && (pos < (arg + arg_size - 1))) {
      if (c == '\n') break;
      *pos = c;
      pos++;
    }
    *pos = ']'; pos++; *pos = '\0';

    fclose(f);

  }

  return RET_OK;
}


static int proc_entries_get(proc_entry_t *entries, char *args, unsigned int arg_size, unsigned int proc_count, unsigned int *proc_found)
{
  DIR *d;
  struct dirent *de;

  d = opendir("/proc");
  if (!d) {
    return RET_FAIL;
  }

  // walk through the /proc dir and search for pids, then read proc data for each pid
  (*proc_found) = 0;
  while (likely((de = readdir(d)) != NULL)) {
    if (likely(likely(*de->d_name > '0') && likely(*de->d_name <= '9'))) {
      if (proc_entry_get(entries + (*proc_found), args + (*proc_found) * arg_size, arg_size, de->d_name)) {
        (*proc_found)++;
        if (unlikely((*proc_found) == proc_count)) // the process array is full
	  break;
      }
    }
  }

  closedir(d);

  return RET_OK;
}


// return cpu stats for all cores
int cpu_sample_get(ring_buffer_t *ring_buffer)
{
  static cpu_jiffies_t diff;
  unsigned long sum;
  unsigned int core, core_count;
  cpu_sample_t *cpu_sample;
  time_t *ts;

  cpu_sample = ring_buffer->cpu_sample + ring_buffer->writer_index * ring_buffer->config->core_count;
  ts = ring_buffer->ts + ring_buffer->writer_index;
  core_count = ring_buffer->config->core_count;

  for (core = 0; core < core_count; core++)
    jiffies_old[core] = jiffies_new[core];

  if (!cpu_jiffies_get(jiffies_new, core_count)) {
    ring_buffer->flags |= FLAG_PROC_READ_ERROR;
    return RET_FAIL;
  }

  time(ts);

  for (core = 0; core < core_count; core++) {

    #define DIFF(entry) diff.entry = jiffies_new[core].entry - jiffies_old[core].entry
    DIFF(user); DIFF(nice); DIFF(system); DIFF(idle); DIFF(iowait); DIFF(irq);
    DIFF(softirq); DIFF(steal); DIFF(guest); DIFF(guest_nice);
    #undef DIFF

    sum = diff.user + diff.nice + diff.system + diff.idle + diff.iowait +
          diff.irq + diff.softirq + diff.steal; 

    // under specific circumstances the sum can be zero (CPU off /c-states)
    // and we need to avoid division by zero
    if (!sum) sum = 1;

    #define STORE(entry) cpu_sample[core].entry = 100 * diff.entry / sum
    STORE(user); STORE(nice); STORE(system); STORE(idle); STORE(iowait);
    STORE(irq); STORE(softirq); STORE(steal); STORE(guest); STORE(guest_nice);
    #undef STORE

  }

  return RET_OK;
}


void toplist_clean(toplist_t *toplist, char *toplist_args, config_t *config)
{
  int tl_index;

  for (tl_index = 0; tl_index < config->toplist_length; tl_index++) {
    toplist[tl_index].pid = 0; toplist[tl_index].ppid = 0;
    toplist[tl_index].utime = 0; toplist[tl_index].stime = 0;
    toplist[tl_index].cutime = 0; toplist[tl_index].cstime = 0;
    toplist[tl_index].guest_time = 0; toplist[tl_index].cguest_time = 0;
    toplist[tl_index].time = 0; toplist[tl_index].sum = 0;
    strcpy(toplist_args + tl_index * config->arg_size, "");
  }
}


// FIXME: moving the whole toplist content with each hit is inefficient ... we want pointers
void toplist_insert(toplist_t *toplist, proc_entry_t *diff, char *diff_args,
		    unsigned long total_sum, config_t *config)
{
  unsigned int tl_i, size;

  for (tl_i = 0; tl_i < config->toplist_length; tl_i++) {
    if (unlikely(diff->sum > toplist[tl_i].sum)) {
      size = config->toplist_length - tl_i - 1;
      if (size > 0) {
        memmove(toplist + tl_i + 1, toplist + tl_i, size * sizeof(toplist_t));
      }
      break;
    }
  }

  if (tl_i < config->toplist_length) {

    #define STORE(entry) toplist[tl_i].entry = 100 * diff->entry / total_sum
    STORE(utime); STORE(stime); STORE(cutime); STORE(cstime); STORE(guest_time); STORE(cguest_time);
    #undef STORE
    toplist[tl_i].sum = diff->sum;
    toplist[tl_i].time = 100 * diff->sum / total_sum;
    toplist[tl_i].arg_tmp = diff_args;
    #define STORE(entry) toplist[tl_i].entry = diff->entry
    STORE(pid); STORE(ppid);
    #undef STORE

  }
}


void toplist_create(toplist_t *toplist, char *toplist_args, proc_entry_t *diff, char **diff_args,
		    unsigned int diff_len, unsigned long total_sum, config_t *config)
{
  unsigned int diff_i, tl_i;

  toplist_clean(toplist, toplist_args, config);

  // sort the whole diff to the toplist
  for (diff_i = 0; diff_i < diff_len; diff_i++) {
    toplist_insert(toplist, diff + diff_i, diff_args[diff_i], total_sum, config);
  }

  // finally clone the arguments
  for (tl_i = 0; tl_i < config->toplist_length; tl_i++) {
    if (toplist[tl_i].pid) {
      strcpy(toplist_args + tl_i * config->arg_size, toplist[tl_i].arg_tmp);
    }
  }
}


// return toplist
int toplist_get(ring_buffer_t *ring_buffer)
{
  unsigned long sum;
  unsigned long total_sum;
  static unsigned int proc_found[2];
  unsigned int old_i, new_i, diff_i;
  int old_pid, new_pid;
  toplist_t *toplist;
  char *toplist_args;

  toplist = ring_buffer->toplist + ring_buffer->writer_index * ring_buffer->config->toplist_length;
  toplist_args = ring_buffer->toplist_args + ring_buffer->writer_index * ring_buffer->config->toplist_length * ring_buffer->config->arg_size;

  proc_entries_new_toggle = 1 - proc_entries_new_toggle;

  if (!proc_entries_get(proc_entries[proc_entries_new_toggle], proc_args[proc_entries_new_toggle],
                        ring_buffer->config->arg_size, ring_buffer->config->proc_count, &proc_found[proc_entries_new_toggle])) {
    ring_buffer->flags |= FLAG_PROC_READ_ERROR;
    return RET_FAIL;
  }

  if (proc_found[proc_entries_new_toggle] == ring_buffer->config->proc_count)
    ring_buffer->flags |= FLAG_PROC_ARRAY_FULL;

  total_sum = old_i = new_i = diff_i = 0;

  // compare the consequent proc samples
  while ((old_i < proc_found[1 - proc_entries_new_toggle]) && (new_i < proc_found[proc_entries_new_toggle])) {

    old_pid = (proc_entries[1 - proc_entries_new_toggle] + old_i)->pid;
    new_pid = (proc_entries[proc_entries_new_toggle] + new_i)->pid;

    if (old_pid == new_pid) {

      #define COPY(entry) (proc_entries_diff + diff_i)->entry = (proc_entries[proc_entries_new_toggle] + new_i)->entry
      COPY(pid); COPY(ppid);
      #undef COPY
      #define DIFF(entry) (proc_entries_diff + diff_i)->entry = (proc_entries[proc_entries_new_toggle] + new_i)->entry - (proc_entries[1 - proc_entries_new_toggle] + old_i)->entry
      DIFF(utime); DIFF(stime); DIFF(cutime); DIFF(cstime); DIFF(guest_time); DIFF(cguest_time);
      #undef DIFF

      proc_args_diff[diff_i] = (proc_args[proc_entries_new_toggle] + new_i * ring_buffer->config->arg_size);

      sum = (proc_entries_diff + diff_i)->utime + (proc_entries_diff + diff_i)->stime +
	    (proc_entries_diff + diff_i)->cutime + (proc_entries_diff + diff_i)->cstime;
      total_sum += sum;

      (proc_entries_diff + diff_i)->sum = sum;

      old_i++; new_i++; diff_i++;
    } else if (old_pid < new_pid) { // old_pid just disappeared
      old_i++;
    } else { // if (old_pid > new_pid)  ... new_pid just appeared
      proc_entries_diff[diff_i] = (proc_entries[proc_entries_new_toggle])[new_i]; // copy new to diff
      proc_args_diff[diff_i] = (proc_args[proc_entries_new_toggle] + new_i * ring_buffer->config->arg_size);

      sum = (proc_entries_diff + diff_i)->utime + (proc_entries_diff + diff_i)->stime +
	    (proc_entries_diff + diff_i)->cutime + (proc_entries_diff + diff_i)->cstime;
      total_sum += sum;

      (proc_entries_diff + diff_i)->sum = sum;

      new_i++; diff_i++;
    }

  }

  toplist_create(toplist, toplist_args, proc_entries_diff, proc_args_diff, diff_i, total_sum, ring_buffer->config);

  return RET_OK;
}


static void cpu_jiffies_destroy()
{
  safe_free(jiffies_new);
  safe_free(jiffies_old);
}


static int cpu_jiffies_init(unsigned int core_count)
{
  jiffies_old = (cpu_jiffies_t *) malloc(core_count * sizeof(cpu_jiffies_t));
  if (jiffies_old) {

    jiffies_new = (cpu_jiffies_t *) malloc(core_count * sizeof(cpu_jiffies_t));
    if (jiffies_new)
      return RET_OK;

  }

  syslog(LOG_ERR, "%s: malloc failed!", __func__);
  cpu_jiffies_destroy();
  return RET_FAIL;
}


static void proc_entries_destroy()
{
  safe_free(proc_args_diff);
  safe_free(proc_args[1]);
  safe_free(proc_args[0]);
  safe_free(proc_entries_diff);
  safe_free(proc_entries[1]);
  safe_free(proc_entries[0]);
}


static int proc_entries_init(unsigned int proc_count, unsigned int arg_size)
{
  proc_entries_new_toggle = 0;

  proc_entries[0] = (proc_entry_t *) malloc(proc_count * sizeof(proc_entry_t));
  if (proc_entries[0]) {

    proc_entries[1] = (proc_entry_t *) malloc(proc_count * sizeof(proc_entry_t));
    if (proc_entries[1]) {

      proc_entries_diff = (proc_entry_t *) malloc(proc_count * sizeof(proc_entry_t));
      if (proc_entries_diff) {

        proc_args[0] = (char *) malloc(proc_count * arg_size * sizeof(char));
        if (proc_args[0]) {

	  proc_args[1] = (char *) malloc(proc_count * arg_size * sizeof(char));
	  if (proc_args[1]) {

	    proc_args_diff = (char **) malloc(proc_count * sizeof(char *));
	    if (proc_args_diff)
	      return RET_OK;

	  }
	}
      }
    }
  }

  syslog(LOG_ERR, "%s: malloc failed!", __func__);
  proc_entries_destroy();
  return RET_FAIL;
}


int proc_init(config_t *config)
{
  if (!cpu_jiffies_init(config->core_count)) {
    return RET_FAIL;
  }

  if (!proc_entries_init(config->proc_count, config->arg_size)) {
    cpu_jiffies_destroy();
    return RET_FAIL;
  }

  return RET_OK;
}


void proc_destroy()
{
  proc_entries_destroy();
  cpu_jiffies_destroy();
}
